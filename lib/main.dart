import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:health_app_test/bloc/auth/auth_bloc.dart';
import 'package:health_app_test/bloc/auth/auth_state.dart';
import 'router/app_router.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<void> main() async {
  if (kReleaseMode) {
    await dotenv.load(fileName: ".env.prod");
  } else {
    await dotenv.load(fileName: ".env.dev");
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final AppRouter _appRouter = AppRouter();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBloc>(
        create: (context) => AuthBloc(),
        child: BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is Disconnected) {
              // Utilisez la clé pour accéder au Navigator et naviguer
              navigatorKey.currentState?.pushReplacementNamed('/login');
            }
          },
          child: MaterialApp(
            navigatorKey: navigatorKey,
            onGenerateTitle: (context) {
              return AppLocalizations.of(context)!.appTitle;
            },
            darkTheme: ThemeData(
                brightness: Brightness.dark,
                primarySwatch: Colors.blue,
                visualDensity: VisualDensity.adaptivePlatformDensity),
            themeMode: ThemeMode.system,
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [
              // 'en' is the language code. We could optionally provide a
              // country code as the second param, e.g.
              // Locale('en', 'US'). If we do that, we may want to
              // provide an additional app_en_US.arb file for
              // region-specific translations.
              Locale('fr', ''),
              Locale('en', ''),
            ],
            theme: ThemeData(
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            onGenerateRoute: (settings) =>
                _appRouter.onGenerateRoute(settings, context),
          ),
        ));
  }
}

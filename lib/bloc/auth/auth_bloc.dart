// lib/bloc/auth_bloc.dart
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(Disconnected()) {
    on<LoginEvent>((event, emit) async {
      emit(Loading());
      debugPrint(event.email as String?);
      debugPrint(dotenv.env['API_URL']);
      if (event.email == 'admin@localhost.test' && event.password == 'test') {
        await Future.delayed(const Duration(seconds: 2));
        emit(Connected(connexion_type: event.connexion_type));
      } else {
        debugPrint("mauvais mot de passe");
        emit(Disconnected());
      }
    });
    on<LogoutEvent>((event, emit) {
      emit(Disconnected());
    });
  }
}

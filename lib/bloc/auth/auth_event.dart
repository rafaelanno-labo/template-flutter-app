import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginEvent extends AuthEvent {
  final String email;
  final String password;
  final String connexion_type;

  LoginEvent(this.email, this.password, this.connexion_type);

  @override
  List<Object> get props => [email, password, connexion_type];
}

class LogoutEvent extends AuthEvent {}

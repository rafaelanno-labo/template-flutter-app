import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

class Connected extends AuthState {
  final String connexion_type;
  Connected({required this.connexion_type});

  @override
  List<Object?> get props => [connexion_type];
}

class Loading extends AuthState {}

class Disconnected extends AuthState {}

import 'package:flutter/material.dart';
import 'package:health_app_test/ui/containers/page_scaffold.dart';
import 'package:health_app_test/ui/pages/home_screen.dart';
import 'package:health_app_test/ui/pages/login_screen.dart';
import 'package:health_app_test/ui/pages/page1.dart';
import 'package:health_app_test/ui/pages/page2.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings settings, BuildContext context) {
    var t = AppLocalizations.of(context);
    debugPrint('${t?.homeTitle} test.');
    final Map<String, Map<String, dynamic>> pageConfiguration = {
      '/home': {
        'screen': const HomeScreen(),
        'drawer': true,
        'title': 'Accueil',
      },
      '/page1': {
        'screen': const PageOne(),
        'drawer': true,
        'title': 'Page Une'
      },
      '/page2': {
        'screen': const PageTwo(),
        'drawer': true,
        'title': 'Page Deux'
      },
      '/login': {'screen': const LoginScreen(), 'drawer': false}
    };

    final routeConfig = pageConfiguration[settings.name];

    // Check if the route configuration exists
    if (routeConfig != null) {
      final Widget screen = routeConfig['screen'] as Widget;
      final bool drawer = routeConfig['drawer'] as bool;
      final String title = routeConfig['title'] as String? ?? '';
      return MaterialPageRoute(
        builder: (_) => drawer
            ? PageScaffold(
                body: screen,
                title: title,
                currentRoute: settings.name ??
                    '/login') // Passer settings.name comme currentRoute
            : screen,
      );
    }

    // Fallback to LoginScreen if route is not defined
    return MaterialPageRoute(builder: (_) => const LoginScreen());
  }
}

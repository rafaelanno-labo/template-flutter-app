import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class HttpService {
  late Dio _dio;

  final baseUrl = dotenv.env['API_URL']; // Remplacez par votre URL de base

  HttpService() {
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl.toString(),
    ));

    initializeInterceptors();
  }

  Future<Response> getRequest(String endpoint) async {
    Response response;
    try {
      response = await _dio.get(endpoint);
    } on DioError catch (e) {
      throw Exception('Erreur lors de la requête GET : $e');
    }
    return response;
  }

  initializeInterceptors() {
    _dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      // Ici, vous pouvez gérer des configurations spécifiques avant chaque requête
      print("Demande sortante: ${options.path}");
      return handler.next(options); //continue
    }, onResponse: (response, handler) {
      // Ici, vous pouvez gérer la réponse
      print("Réponse entrante: ${response.statusCode}");
      return handler.next(response); // continue
    }, onError: (DioError e, handler) {
      // Ici, vous pouvez gérer les erreurs
      print("Erreur: ${e.message}");
      return handler.next(e); //continue
    }));
  }
}

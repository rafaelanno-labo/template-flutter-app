// lib/ui/pages/home/home_screen.dart
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:health_app_test/bloc/auth/auth_bloc.dart';
import 'package:health_app_test/bloc/auth/auth_event.dart';
import 'package:health_app_test/bloc/auth/auth_state.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var t = AppLocalizations.of(context);
    Locale myLocale = Localizations.localeOf(context);
    return Scaffold(
      body: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  t!.welcome,
                  style: const TextStyle(
                      fontSize: 24, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 20),
                if (state is Connected) ...[
                  Text('Vous êtes connecté $myLocale',
                      style: const TextStyle(color: Colors.green)),
                ] else ...[
                  const Text('Vous n\'êtes pas connecté.',
                      style: TextStyle(color: Colors.red)),
                ],
                ElevatedButton(
                  onPressed: () {
                    BlocProvider.of<AuthBloc>(context).add(LogoutEvent());
                  },
                  child: const Text('Deconnexion'),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

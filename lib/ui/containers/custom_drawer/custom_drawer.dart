import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:health_app_test/bloc/auth/auth_bloc.dart';
import 'package:health_app_test/bloc/auth/auth_state.dart';

class CustomDrawer extends StatelessWidget {
  final String currentRoute;

  const CustomDrawer({
    super.key,
    required this.currentRoute,
  });
  @override
  Widget build(BuildContext context) {
    var t = AppLocalizations.of(context);
    bool isActive(String route) => currentRoute == route;
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is Connected && state.connexion_type == 'type_1') {
          return _buildCustomDrawer(context, t, isActive, state);
        } else {
          // Fallback drawer content when the condition is not met
          return _buildDefaultDrawer(context, t, isActive);
        }
      },
    );
  }

  Drawer _buildCustomDrawer(BuildContext context, AppLocalizations? t,
      bool Function(String) isActive, AuthState state) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            // Custom list items for when the condition is true
            _buildListItem(
              icon: Icons.home,
              text: t!.homeTitle,
              isActive: isActive('/home'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/home'),
            ),
            _buildListItem(
              icon: Icons.message,
              text: 'Messages',
              isActive: isActive('/page1'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/page1'),
            ),
            _buildListItem(
              icon: Icons.settings,
              text: 'Parametres',
              isActive: isActive('/page2'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/page2'),
            )
          ],
        ),
      ),
    );
  }

  Drawer _buildDefaultDrawer(BuildContext context, AppLocalizations? t,
      bool Function(String) isActive) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            _buildListItem(
              icon: Icons.home,
              text: t!.homeTitle,
              isActive: isActive('/home'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/home'),
            ),
            // Add more list items for the default drawer content
          ],
        ),
      ),
    );
  }

  Widget _buildListItem({
    required IconData icon,
    required String text,
    required bool isActive,
    required VoidCallback onTap,
  }) {
    return ListTile(
      leading: Icon(icon),
      title: Text(text),
      tileColor: isActive
          ? Colors.blueGrey[100]
          : null, // Change la couleur de fond si actif
      onTap: onTap,
    );
  }
}

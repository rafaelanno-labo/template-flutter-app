// lib/ui/containers/page_scaffold/page_scaffold.dart
import 'package:flutter/material.dart';
import 'package:health_app_test/ui/containers/custom_drawer/custom_drawer.dart';

class PageScaffold extends StatelessWidget {
  final Widget body;
  final String title;
  final String currentRoute; // Ajout de cette ligne

  const PageScaffold({
    super.key,
    required this.body,
    this.title = '', // Optional if you want to set it from outside
    required this.currentRoute, // Ajout de cette ligne
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      drawer: CustomDrawer(
        currentRoute: currentRoute,
      ),
      body: body,
    );
  }
}

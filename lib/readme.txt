Le but ici est de faire un boilerplate contenant :
- système d'authentification
- système de route
- système de layout(Connecté pas, connecté)

prompt :

fais toi passer pour un expert en flutter ca fait 5 ans que tu as travailler sur des application.

implement un systeme d'authentification de test pour une application flutter.
je souhaite que tu cree deux layout reutilisable. ConnectedScreen et NonConnectedScreen.
Connected screen contien un drawer avec trois titre permettant de naviguer dans l'application quand on est connecté. 

ConnectedScreen sont des pages parente des 3 page du menu drawer.

pour ton système d'authentification utilise flutter bloc pour gerer l'etat de connexion et de deconnexion.

Architecture :

lib/
|-- bloc/
|   |-- auth/
|   |   |-- auth_bloc.dart
|   |   |-- auth_event.dart
|   |   |-- auth_state.dart
|-- models/
|   |-- user.dart
|-- repositories/
|   |-- user_repository.dart
|-- ui/
|   |-- components/
|   |   |-- buttons/
|   |   |   |-- custom_button.dart
|   |   |-- forms/
|   |       |-- custom_text_field.dart
|   |-- containers/
|   |   |-- custom_drawer/
|   |   |   |-- custom_drawer.dart
|   |   |-- page_scaffold/
|   |       |-- page_scaffold.dart
|   |-- pages/
|       |-- home/
|       |   |-- home_screen.dart
|       |-- login/
|           |-- login_screen.dart
|-- router/
|   |-- app_router.dart
|-- main.dart
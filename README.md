[![fr-Fr](https://img.shields.io/badge/lang-fr--Fr-green.svg)](./README.fr.md)

# Flutter App Template

Welcome to our Flutter Template, a configurable and ready-to-use project base designed to accelerate the development of your mobile apps. This template includes a set of essential features built-in, allowing you to start with a solid foundation.

## Features

- **Built-in Internationalization**: Supports multiple languages out of the box, facilitating the global deployment of your application.
- **Authentication System**: Robust authentication module for secure user management.
- **Integration of a store to manage global variables**: Uses state management for efficient and centralized management of states within the application.
- **Layout System**: Pre-designed layouts for visual consistency and easy maintenance.
- **Menu System**: Smooth navigation with an intuitive and customizable menu system.
- **Router**: Advanced route management for precise control of history and transitions between screens.

## Getting Started

To use this template, follow the instructions below:

### Prerequisites

Ensure you have Flutter installed on your machine. For more information on installing Flutter, refer to the [official Flutter documentation](https://flutter.dev/docs/get-started/install).

### Installation

#### Clone the repo
   ```bash
   git clone https://gitlab.com/rafaelanno-labo/template-flutter-app.git
   ```
#### Install dependences
   ```bash
   flutter pub get
   ```

#### Launch the app on linux
    ```bash
    flutter run -d linux
    ```

#### Launch the app on an android device
   
   ```bash
   # list all emulators
   flutter emulators
   ```
   ```bash
   # launch the emulators
   flutter emulators --launch id
   ```
   ```bash
   # lanch the app
   flutter run
   ```
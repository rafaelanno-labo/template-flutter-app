import 'package:flutter_test/flutter_test.dart';
import 'package:health_app_test/main.dart';

void main() {
  testWidgets('Check if the default page is the login page',
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('Se connecter'), findsOneWidget);
  });
}

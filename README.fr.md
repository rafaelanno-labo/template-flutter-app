[![en](https://img.shields.io/badge/lang-en-green.svg)](./README.md)

# Flutter App Template

Bienvenue dans notre Template Flutter, une base de projet configurable et prête à l'emploi pour accélérer le développement de vos applications mobiles. Ce template inclut un ensemble de fonctionnalités essentielles intégrées, vous permettant de démarrer avec une fondation solide.

## Caractéristiques

- **Internationalisation intégrée** : Prend en charge plusieurs langues dès le départ, facilitant le déploiement global de votre application.
- **Système d'authentification** : Module d'authentification robuste permettant une gestion sécurisée des utilisateurs.
- **Integration d'un store pour gérer des variables globales** : Utilisation de state management pour une gestion efficace et centralisée des états au sein de l'application.
- **Système de layout** : Des layouts préconçus pour une cohérence visuelle et une maintenance facile.
- **Système de menu** : Navigation fluide avec un système de menu intuitif et personnalisable.
- **Routeur** : Gestion avancée des routes pour un contrôle précis de l'historique et des transitions entre les écrans.

## Commencer

Pour utiliser ce template, suivez les instructions ci-dessous :

### Prérequis

Assurez-vous d'avoir Flutter installé sur votre machine. Pour plus d'informations sur l'installation de Flutter, consultez la [documentation officielle de Flutter](https://flutter.dev/docs/get-started/install).

### Installation

#### Clonez ce repository
   ```bash
   git clone https://gitlab.com/rafaelanno-labo/template-flutter-app.git
   ```
#### Installez les dépendances
   ```bash
   flutter pub get
   ```

#### Lancez l'application sur linux
    ```bash
    flutter run -d linux
    ```

#### Lancez l'application sur android
   
   ```bash
   # lister les emulators
   flutter emulators
   ```
   ```bash
   # lister les emulators
   flutter emulators --launch id
   ```
   ```bash
   # lancer l'app sur l'emulateur android
   flutter run
   ```